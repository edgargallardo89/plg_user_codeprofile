AdBox Express
======
# Plugin - Código de verificación en registro

Plugin que permite validar a los usuarios en el registro de acuerdo con el código de verificación asignado previamente.

## Instalación.

Requisitos:

* Joomla >= 3.x.x

### Procedimiento:

* Abrir el área de administración www.Direccion_del_proyecto.com/administrator
* Ir al menú superior y presionar "Extensions" seguido de "Manage" lo cual te llevara a la página de instalación.
* Iremos a la pestaña "Upload package file" y seleccionaremos nuestro archivo *.zip
Esto instalara tanto las tablas necesarias como todos los archivos necesitados por el plugin.

## Base de datos:


### Crea tablas: 

Instala:
* user_code (contiene la relación de username y códigos de verificación, se generan con estatus 1, una vez validados en su registro el estatus cambia a 0)


## Dependencias 
 
 * Habilitar el campo código en el apartado de registro principal, esta acción se realiza desde la configuración del plugin user_profile.
