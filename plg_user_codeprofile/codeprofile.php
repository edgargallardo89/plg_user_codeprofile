<?php
/**
 * @copyright	Copyright (c) 2017 codeprofile. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

/**
 * user - codeprofile Plugin
 *
 * @package		Joomla.Plugin
 * @subpakage	codeprofile.codeprofile
 */
class plgusercodeprofile extends JPlugin {

	/**
	 * Constructor.
	 *
	 * @param 	$subject
	 * @param	array $config
	 */
	function __construct(&$subject, $config = array()) {
		// call parent constructor
		parent::__construct($subject, $config);
	}

	 function onUserBeforeSave($oldUser,$isnew,$newUser) {
	 	//var_dump($newUser['profile']['code']); die();
	 	if($isnew){
	 		$result = $this->getUserCode($newUser['username'],$newUser['profile']['code']);

	 		if(!$result){
	 			echo "<script>alert('no se puede completar el registro')</script>";
	 			echo "<script>window.location.href = '".JUri::base()."'</script>";
	 			die();
	 		}else{
	 			$this->updateStatusUserCode($newUser['username'],$newUser['profile']['code']);
	 		}
	 	}
	}
	


	private function getUserCode($username, $code){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
    	->select(array('*'))
    	->from($db->quoteName('#__core_user_code', 'a'))
    	->where($db->quoteName('username')." = ".$db->quote($username))
    	->where($db->quoteName('status') . ' = 1')
    	->where($db->quoteName('code')." = ".$db->quote($code));

    	$db->setQuery($query);
    	$result = $db->loadObjectList();
    	return $result;
	}

	private function updateStatusUserCode($username, $code){
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$timestamp = date('Y-m-d G:i:s');

	$fields = array(
    $db->quoteName('status') . ' = 0',
	$db->quoteName('updated_at') . ' = ' . $db->quote($timestamp));

	$conditions = array(
    $db->quoteName('username')." = ".$db->quote($username),
    $db->quoteName('code')." = ".$db->quote($code));

	$query->update($db->quoteName('#__core_user_code'))->set($fields)->where($conditions);

	$db->setQuery($query);

	$result = $db->execute();
	}
}